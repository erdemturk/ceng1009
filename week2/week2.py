import turtle

win = turtle.Screen()
rafael = turtle.Turtle()

numberOfSides = int(input("Please enter the number of sides:"))
sideLength = int(input("Please enter the side length:"))
angle = 360 / numberOfSides
color = input("Please enter the color:")
fillColor = input("Please enter the fill color:")

rafael.color(color)
rafael.fillcolor(fillColor)
rafael.begin_fill()
for i in range(numberOfSides):
    rafael.forward(sideLength)
    rafael.left(angle)
rafael.end_fill()
win.exitonclick()